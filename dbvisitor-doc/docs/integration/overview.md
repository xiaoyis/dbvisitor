---
id: overview
sidebar_position: 1
title: 概要
description: dbVisitor ORM 工具和其它框架整合使用。
---

- **[Spring 框架 Xml 方式整合](./with-spring.md)**
- **[SpringBoot 框架注解方式整合](./with-springboot.md)**
- **[Google Guice 框架整合](./with-guice.md)**
- **[Hasor 框架整合](./with-hasor.md)**

